/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.albumproject.dao;

import com.fhang.albumproject.helper.DatabaseHelper;
import com.fhang.albumproject.model.ReportSale;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Natthakritta
 */
public class SaleDao {
    public List<ReportSale> getDayReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m-%d\",InvoiceDate) as period, SUM(Total) as total FROM invoices LEFT JOIN invoice_items ON invoices.InvoiceId = invoice_items.InvoiceId GROUP BY period ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale employee = ReportSale.fromRS(rs);
                list.add(employee);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
     public List<ReportSale> getMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\",InvoiceDate) as period, SUM(Total) as total FROM invoices LEFT JOIN invoice_items ON invoices.InvoiceId = invoice_items.InvoiceId GROUP BY period ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale employee = ReportSale.fromRS(rs);
                list.add(employee);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     public List<ReportSale> getMonthReportByYear(String year) {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%m\", InvoiceDate) as period, SUM(Total) as total FROM invoices LEFT JOIN invoice_items ON invoices.InvoiceId = invoice_items.InvoiceId WHERE strftime(\"%Y\", InvoiceDate) = \""+year+"\" GROUP BY period ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale employee = ReportSale.fromRS(rs);
                list.add(employee);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
