/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.albumproject.service;

import com.fhang.albumproject.dao.SaleDao;
import com.fhang.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author Natthakritta
 */
public class ReportService {
    public List<ReportSale> getreportSaleByDay(){
        SaleDao saleDao = new SaleDao();
        return saleDao.getDayReport();
    }
     public List<ReportSale> getreportSaleByMonth(){
        SaleDao saleDao = new SaleDao();
        return saleDao.getMonthReport();
    }
      public List<ReportSale> getreportSaleByMonth(String year){
        SaleDao saleDao = new SaleDao();
        return saleDao.getMonthReportByYear(year);
    }
}
